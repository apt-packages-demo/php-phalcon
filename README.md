# php-phalcon

Web framework delivered as a C extension. https://phalcon.io

[![PHPPackages Rank](http://phppackages.org/p/phalcon/cphalcon/badge/rank.svg)](http://phppackages.org/p/phalcon/cphalcon)
[![PHPPackages Referenced By](http://phppackages.org/p/phalcon/cphalcon/badge/referenced-by.svg)](http://phppackages.org/p/phalcon/cphalcon)
* https://packagist.org/packages/phalcon/cphalcon

## Official documentation
* [*Installation*](https://docs.phalcon.io/latest/en/installation)
  * Also on Debian Sury https://gitlab.com/apt-packages-demo/php-phalcon
* [*Installation*](https://phalcon-php-framework-documentation.readthedocs.io/en/latest/reference/install.html)
  (readthedocs.io)

## Unofficial documentation
* [*Phalcon (framework)*](https://en.m.wikipedia.org/wiki/Phalcon_(framework))
  (Wikipedia)
* [*How To Install Phalcon PHP on Debian 9 (Stretch)*
  ](https://tecadmin.net/install-phalcon-php-on-debian-9-stretch/)
  2018-09 Rahul
* [*How To Install Phalcon PHP Framework on Ubuntu 18.04 & 16.04*
  ](https://tecadmin.net/install-phalcon-php-framework-on-ubuntu/)
  2018-08 Rahul

### Tutorial
* [phalcon tutorial](https://www.google.com/search?q=phalcon+tutorial)
* [*Phalcon Tutorial*](https://www.tutorialspoint.com/phalcon/index.htm)